package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/SergeySlonimsky/mate-cli/internal/generate"
	"gitlab.com/SergeySlonimsky/mate-cli/internal/version"
	"os"
)

func main() {
	cmd := &cobra.Command{
		Use:   "mate-cli",
		Short: "CLI tool for mate project",
	}

	cmd.AddCommand(version.Command())
	cmd.AddCommand(generate.Command())

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
