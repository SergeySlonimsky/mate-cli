package version

import (
	"errors"
	"github.com/spf13/cobra"
	"runtime/debug"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use:     "version",
		Example: "mate-cli version",
		Short:   "Print tool version",
		RunE: func(cmd *cobra.Command, args []string) error {
			info, ok := debug.ReadBuildInfo()
			if !ok {
				return errors.New("invalid version")
			}

			cmd.Println(info.Main.Version)

			return nil
		},
	}
}
