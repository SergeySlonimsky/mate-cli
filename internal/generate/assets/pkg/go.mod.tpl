module gitlab.com/project-mate/<%service-name%>-service/pkg/sdk

go 1.19

require (
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
)

