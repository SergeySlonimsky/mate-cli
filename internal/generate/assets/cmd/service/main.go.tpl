package main

import (
	"log"

	"gitlab.com/project-mate/libs/core/grpc"
	"gitlab.com/project-mate/libs/core/httpserver"
	"gitlab.com/project-mate/libs/core/server"
	"gitlab.com/project-mate/<%service-name%>-service/internal/config"
	"gitlab.com/project-mate/<%service-name%>-service/internal/handler/http/admin"
	"gitlab.com/project-mate/<%service-name%>-service/internal/handler/http/company"
	"gitlab.com/project-mate/<%service-name%>-service/internal/handler/http/app"
)

func main() {
	container, onClose := di.LoadContainer(config.NewConfig, config.NewServices)

	defer onClose()

	httpServer := httpserver.New(container.GetConfig(), container.GetLogger())
	grpcServer := grpc.New(container.GetConfig(), container.GetLogger())

	httpServer.Add("admin", admin.NewHandler(container), httpserver.AuthMiddleware(user.TypeAdmin))
	httpServer.Add("company", company.NewHandler(container), httpserver.AuthMiddleware(user.TypeCompany))
	httpServer.Add("app", app.NewHandler(container), httpserver.AuthMiddleware(user.TypeApp))

	log.Panic(server.NewServer(httpServer, grpcServer).Run())
}
