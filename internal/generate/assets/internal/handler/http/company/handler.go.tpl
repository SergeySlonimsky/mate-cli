package company

import (
	"gitlab.com/project-mate/<%service-name%>-service/internal/config"
	"gitlab.com/project-mate/<%service-name%>-service/internal/handler/http/company/dummy"
    "gitlab.com/project-mate/libs/core/di"
    "gitlab.com/project-mate/libs/core/httpserver"
)

type Handler struct {
	dummy *dummy.Handler
}


func NewHandler(container *di.Container[*config.Config, *config.Services]) *Handler {
	return &Handler{
		dummy: dummy.NewHandler(container),
	}
}

func (h *Handler) Init(g *httpserver.Group) {
	h.dummy.Init(g.Group("/dummy"))
}
