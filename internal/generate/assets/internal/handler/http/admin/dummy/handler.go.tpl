package dummy

import (
    "gitlab.com/project-mate/libs/core/di"
    "gitlab.com/project-mate/libs/core/httpserver"
    "gitlab.com/project-mate/<%service-name%>-service/internal/config"
)

type Handler struct {
}

func NewHandler(container *di.Container[*config.Config, *config.Services]) *Handler {
    return &Handler{}
}

func (h *Handler) Init(router *httpserver.Group) {
	router.GET("/test", h.GetTest)
}

func (h *Handler) GetTest(req *httpserver.RequestContext) *httpserver.Response {
    return httpserver.NewJSONResponse("ok")
}
