package config

import (
	"go.uber.org/zap"
	"log"
	"gitlab.com/project-mate/libs/database"
)

type Services struct {
	DB database.DBConn

	// app services
}

func NewServices(cnf *Config, logger *zap.Logger) (*Services, func()) {
	db, err := database.New(logger)
	if err != nil {
		log.Panicln(err)
	}

	return &Services{
			DB: db,
		}, func() {
			db.Close()
		}
}
