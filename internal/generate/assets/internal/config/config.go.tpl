package config

import (
	"gitlab.com/project-mate/libs/core/di"
)

type Config struct {
	*di.Config
	// app configs
}

func NewConfig() *Config {
	return &Config{
		Config: di.NewConfig(),
	}
}
