package repo

import (
    "context"

    "gitlab.com/project-mate/<%service-name%>-service/internal/model"
    "gitlab.com/project-mate/libs/database"
)

func GetDummy(ctx context.Context, db database.Query, field string) (*model.Dummy, error) {
	dummy := &model.Dummy{}

	qb := database.QueryBuilder().
		Select("*").
		From("dummy").
		Where(sq.Eq{"field_1": field})

	err := db.Get(ctx, dummy, qb)

	return dummy, err
}
