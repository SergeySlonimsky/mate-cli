package generate

import (
	"bytes"
	"embed"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"io/fs"
	"os"
	"os/exec"
	"strings"
)

//go:embed assets/*
var assets embed.FS

func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "generate",
		Short:   "Generate new service template",
		Example: "mate-cli generate <service name>",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("need to provide service name")
			}

			svcName := args[0]

			if strings.HasSuffix(svcName, "-service") {
				return errors.New("invalid service name, should be without 'service' suffix")
			}

			svcFullName := fmt.Sprintf("%s-service", svcName)

			cmd.Println("Creating service folder...")

			err := os.Mkdir(svcFullName, 0777)

			if err != nil {
				return fmt.Errorf("creating service folder: %w", err)
			}

			cmd.Println("Copying assets...")

			fs.WalkDir(assets, "assets", func(path string, d fs.DirEntry, err error) error {
				if d.IsDir() {
					if d.Name() == "assets" {
						return nil
					}
					err := os.Mkdir(strings.Replace(path, "assets", svcFullName, 1), 0777)

					if err != nil {
						return fmt.Errorf("creating assets subfolder %s: %w", path, err)
					}
				} else {
					f, err := assets.ReadFile(path)

					if err != nil {
						return fmt.Errorf("reading file %s: %w", path, err)
					}

					output := bytes.Replace(f, []byte("<%service-name%>"), []byte(svcName), -1)

					fileName := strings.Replace(path, "assets", svcFullName, 1)
					if strings.HasSuffix(fileName, ".tpl") {
						fileName = strings.TrimSuffix(fileName, ".tpl")
					}

					err = os.WriteFile(fileName, output, 0666)
					if err != nil {
						return fmt.Errorf("writing to file %s: %w", output, err)
					}
				}

				return nil
			})

			if err := os.Chdir(svcFullName); err != nil {
				return fmt.Errorf("chdir %s: %w", svcName, err)
			}

			cmd.Println("Initializing go module...")

			if err := exec.CommandContext(
				cmd.Context(),
				"go",
				"mod",
				"init",
				fmt.Sprintf("gitlab.com/project-mate/%s", svcFullName),
			).Run(); err != nil {
				return fmt.Errorf("execucute go mod init command: %w", err)
			}

			if err := exec.CommandContext(cmd.Context(), "make", "vendor").Run(); err != nil {
				return fmt.Errorf("execucute make vendor command: %w", err)
			}

			cmd.Println("Initializing git repository...")

			if err := exec.CommandContext(cmd.Context(), "git", "init", "-b", "master").Run(); err != nil {
				return fmt.Errorf("execucute git init command: %w", err)
			}

			if err := exec.CommandContext(
				cmd.Context(),
				"git",
				"remote",
				"add",
				"origin",
				fmt.Sprintf("git@gitlab.com:project-mate/%s.git", svcFullName),
			).Run(); err != nil {
				return fmt.Errorf("execucute git origin command: %w", err)
			}

			if err := exec.CommandContext(cmd.Context(), "git", "add", ".").Run(); err != nil {
				return fmt.Errorf("execucute git add command: %w", err)
			}

			cmd.Println("Copying env file...")

			if err := exec.CommandContext(cmd.Context(), "cp", ".env.dist", ".env").Run(); err != nil {
				return fmt.Errorf("copy .env file: %w", err)
			}

			cmd.Println("Done!")

			return nil
		},
	}

	return cmd
}
